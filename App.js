import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';

// Use Navigation Ver. 5.7.0
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

// Screen
import HomePage from './src/screens/homePage/HomePage';
import AddNewNote from './src/screens/addNewNote/AddNewNote';
import DetailNote from './src/screens/detailNote/DetailNote';
import Onboarding from './src/screens/onboarding/Onboarding';
import Summary from './src/screens/summary/Summary';
import Akun from './src/screens/akun/Akun';
import Login from './src/screens/login/Login';

const Stack = createStackNavigator();

function StackNav(props) {
  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Onboarding" headerMode={'none'}>
          <Stack.Screen name="Onboarding" component={Onboarding} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="HomePage" component={HomePage} />
          <Stack.Screen name="Akun" component={Akun} />
          <Stack.Screen name="Summary" component={Summary} />
          <Stack.Screen name="AddNewNote" component={AddNewNote} />
          <Stack.Screen name="DetailNote" component={DetailNote} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
}

export default StackNav;

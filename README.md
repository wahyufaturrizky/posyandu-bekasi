# Posyandu Bekasi App By Wahyu Fatur Rizki | wahyufaturrizkyy@gmail.com

## Requirements

- NodeJS
  - Installed NodeJS `>=12.0.0 <15.0.0`
- Yarn
  - Installed Yarn `^1.20.0`
- Android
  - Installed JDK `^8`
  - Installed Android SDK
  - Installed Android NDK Version 21.0.6113669
  - Running Android Emulator / Android Device
- iOS
  - MacOS
  - Installed cocoapod (1.10.1 or latest)
  - Xcode

## Development

### Visual Studio Code

please add eslint auto fix on vscode setting

```json
"editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
}
```

### Installation

1. run `yarn install` to install all package
2. run script `yarn android:run-win` to run android

### Android

1. run `yarn android:run-win` to running development env

```
yarn android:clean-win -> to clean build on app/android/build
yarn android:shake -> to show react native development option on devices or emulators
```

### iOS

1. install pod dependencies on `ios` folder with `pod install`
2. run `yarn ios:run` to running development env

### Explained running script on `package.json`

- `yarn start:clear` : start metro with clear cache
- `yarn test` : watch all testing on `src` folder
- `yarn watch` : watch all linter, prettier and type
- `yarn checker` : check all codebase on `src` folder
- `yarn formatter` : reformatting all codebase on `src` if you got an error on lint
- `yarn generate-env` : generate all types from `.env.*` file

List suffix

- debug
- development
- production

We have 3 release variant

- debug
- developmentrelease
- release

for example if you want to run it with `release` variant you can use `npx` or `react-native-cli` with

```
ENVFILE=.env.staging npx react-native run-android --variant=stagingRelease --appIdSuffix=staging
```

### Pre-Commit (Husky)

We're using a pre-commit script to check linter and unit testing with `yarn checker`, if you got stuck on linter please kindly run `yarn formatter` to automatically fixing format your code.

```
Your commit message should be contain type must be one of [build, chore, ci, docs, feat, fix, perf, refactor, revert, style, test]

feat: feature
perf: performance

example `fix: base layout`
```

## Release

we're using npm version for package.json versioning and react-native-version for android and ios versioning

### Binary

1. create branch `release/v{new version}` example `release/v1.0.2`
2. run npm version script `npm --no-git-tag-version version ${semantic}` eg: major, minor or patch
3. run react-native-postversion script `yarn run release:binary`
4. commit changes `git add . && git commit --amend`
5. push branch and deploy on appcenter
6. git tag -a v1.0.4 -m "Release v.1.0.4"
7. git push origin <tag_name> example `git push origin v1.0.2`
8. Create Pull Request

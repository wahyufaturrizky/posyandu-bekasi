import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';
import {ColorsPrimary, ColorBaseGray} from '../../styles/Colors';

const BottomTab = (props) => {
  const titlePage = props.route.params?.titlePage;

  const gotoAkun = () => {
    props.navigation.navigate('Akun', {titlePage: 'Akun Saya', ...props});
  };

  const gotoHome = () => {
    props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'HomePage', params: {titlePage: 'Beranda'}}],
      }),
    );
  };

  const gotoSummary = () => {
    props.navigation.navigate('Summary', {titlePage: 'Laporan', ...props});
  };

  return (
    <View
      style={[
        styles.shadow,
        {
          backgroundColor: '#FDFEFF',
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          paddingVertical: 4,
        },
      ]}>
      <TouchableOpacity onPress={gotoHome}>
        <View style={{alignItems: 'center'}}>
          <Icon
            name="home"
            size={24}
            color={
              titlePage === 'Beranda'
                ? ColorsPrimary.bluesoft
                : ColorBaseGray.gray400
            }
          />
          <Text style={{color: ColorBaseGray.gray600}}>Beranda</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={gotoSummary}>
        <View style={{alignItems: 'center'}}>
          <Icon
            name="filetext1"
            size={24}
            color={
              titlePage === 'Laporan'
                ? ColorsPrimary.bluesoft
                : ColorBaseGray.gray400
            }
          />
          <Text style={{color: ColorBaseGray.gray600}}>Laporan</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={gotoAkun}>
        <View style={{alignItems: 'center'}}>
          <Icon
            name="user"
            size={24}
            color={
              titlePage === 'Akun Saya'
                ? ColorsPrimary.bluesoft
                : ColorBaseGray.gray400
            }
          />
          <Text style={{color: ColorBaseGray.gray600}}>Akun</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default BottomTab;

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
  },
});

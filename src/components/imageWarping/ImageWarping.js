import React from 'react';
import {StyleSheet, Image, View, Dimensions} from 'react-native';
import {ColorsPrimary} from '../../styles/Colors';

const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;

export const ImageWarping = (props) => {
  return (
    <View style={styles.circleContainer}>
      <Image
        style={styles.styleImage}
        resizeMode={props.resizeMode}
        source={props.source}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  circleContainer: {
    borderRadius:
      Math.round(
        Dimensions.get('window').width + Dimensions.get('window').height,
      ) / 2,
    width: Dimensions.get('window').width * 0.6,
    height: Dimensions.get('window').width * 0.6,
    backgroundColor: ColorsPrimary.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleImage: {
    width: deviceWidth * 0.5,
    height: deviceHeight * 0.5,
  },
});

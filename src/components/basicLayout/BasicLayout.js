import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {ColorsPrimary, ColorBaseGray} from '../../styles/Colors';

export const BasicLayout = (props) => {
  return props.isWithoutScroll ? (
    <View style={styles.container}>{props.children}</View>
  ) : (
    <ScrollView>
      <View refreshControl={props.refreshControl} style={styles.container}>
        {props.children}
      </View>
    </ScrollView>
  );
};

export const BasicLayoutNotCentered = (props) => {
  return props.isWithoutScroll ? (
    <View style={styles.containerNotCentered}>{props.children}</View>
  ) : (
    <ScrollView>
      <View
        refreshControl={props.refreshControl}
        style={styles.containerNotCentered}>
        {props.children}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColorsPrimary.bluesoft,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerNotCentered: {
    flex: 1,
    backgroundColor: ColorBaseGray.gray200,
  },
});

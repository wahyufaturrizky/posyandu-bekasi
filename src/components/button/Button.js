import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {ColorsPrimary} from '../../styles/Colors';

export const ButtonPrimary = (props) => {
  return (
    <TouchableOpacity
      style={[styles.button, props.customeStyle]}
      onPress={props.onPress}>
      <Text style={styles.buttonText}>{props.textButton}</Text>
    </TouchableOpacity>
  );
};

export const ButtonSecondary = (props) => {
  return (
    <TouchableOpacity
      style={[styles.buttonSecondary, props.customeStyle]}
      onPress={props.onPress}>
      <Text style={styles.buttonTextSecondary}>{props.textButton}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
    color: ColorsPrimary.bluesoft,
  },
  button: {
    borderRadius: 24,
    width: '90%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ColorsPrimary.white,
  },
  buttonTextSecondary: {
    fontSize: 17,
    textAlign: 'center',
    color: ColorsPrimary.white,
  },
  buttonSecondary: {
    borderRadius: 24,
    width: '90%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: ColorsPrimary.bluesoft,
  },
});

import React from 'react';
import {Header} from 'react-native-elements';
import {materialColors} from 'react-native-typography';

export const AppBar = (props) => {
  return (
    <Header
      containerStyle={{
        backgroundColor: '#fff',
      }}
      leftComponent={{
        icon: 'arrow-back-ios',
        color: materialColors.blackPrimary,
        onPress: props.onPress,
      }}
      centerComponent={{
        text: `${props.titlePage}`,
        style: {color: materialColors.blackPrimary},
      }}
    />
  );
};

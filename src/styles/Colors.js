export const ColorsPrimary = {
  black: '#000',
  white: '#FFF',
  whiteOpacity: 'rgba(255, 255, 255, 0.5)',
  bluesoft: '#69CCF3',
  blueDark: '#2BB7EE',
  redSoft: '#ff4d4d',
};

export const ColorBaseGray = {
  gray100: '#FAFAFA',
  gray200: '#F5F5F5',
  gray300: '#EBEBEB',
  gray400: '#BDBDBD',
  gray500: '#9E9E9E',
  gray600: '#555555',
  gray700: '#212121',
};

import axios from 'axios';
import qs from 'qs';
import Config from 'react-native-config';
/**
 * @author wahyu fatur rizki | https://gitlab.com/wahyufaturrizky
 * @return { obj }
 * Retrieve Data
 * return async
 */

export const postLogin = (dataForm) => {
  let dataFormValue = qs.stringify(dataForm);
  let config = {
    method: 'post',
    url: `${Config.REACT_APP_URL_POSYANDU}/login`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: dataFormValue,
  };

  return axios(config);
};

export const postNewSummary = (dataForm, dataHeaders) => {
  let dataFormValue = qs.stringify(dataForm);
  let config = {
    method: 'post',
    url: `${Config.REACT_APP_URL_POSYANDU}/api/laporan`,
    headers: {
      Authorization: `${dataHeaders.tokenType} ${dataHeaders.accessToken}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: dataFormValue,
  };

  return axios(config);
};

export const updateNamePosyandu = (dataForm, dataHeaders) => {
  let dataFormValue = qs.stringify(dataForm);
  let config = {
    method: 'put',
    url: `${Config.REACT_APP_URL_POSYANDU}/api/profile/nama`,
    headers: {
      Authorization: `${dataHeaders.tokenType} ${dataHeaders.accessToken}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: dataFormValue,
  };

  return axios(config);
};

export const updateDistrictAndSubdistrict = (dataForm, dataHeaders) => {
  let dataFormValue = qs.stringify(dataForm);
  let config = {
    method: 'put',
    url: `${Config.REACT_APP_URL_POSYANDU}/api/profile/kode`,
    headers: {
      Authorization: `${dataHeaders.tokenType} ${dataHeaders.accessToken}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: dataFormValue,
  };

  return axios(config);
};

export const getSummary = (dataForm, dataHeaders) => {
  let dataFormValue = qs.stringify(dataForm);
  let config = {
    method: 'get',
    url: `${Config.REACT_APP_URL_POSYANDU}/api/laporan?kode_kelurahan=${dataHeaders.kode_kelurahan}&rw=${dataHeaders.rw}`,
    headers: {
      Authorization: `${dataHeaders.tokenType} ${dataHeaders.accessToken}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: dataFormValue,
  };

  return axios(config);
};

export const getSubDistrict = (dataHeaders) => {
  let config = {
    method: 'get',
    url: `${Config.REACT_APP_URL_POSYANDU}/api/kelurahan`,
    headers: {
      Authorization: `${dataHeaders.tokenType} ${dataHeaders.accessToken}`,
    },
  };

  return axios(config);
};

export const getDistrict = (dataHeaders) => {
  let config = {
    method: 'get',
    url: `${Config.REACT_APP_URL_POSYANDU}/api/kecamatan`,
    headers: {
      Authorization: `${dataHeaders.tokenType} ${dataHeaders.accessToken}`,
    },
  };

  return axios(config);
};

export const deleteSummaryById = (dataHeaders, dataForm, id) => {
  let dataFormValue = qs.stringify(dataForm);
  let config = {
    method: 'get',
    url: `${Config.REACT_APP_URL_POSYANDU}/api/delete/laporan?id=${id}`,
    headers: {
      Authorization: `${dataHeaders.tokenType} ${dataHeaders.accessToken}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: dataFormValue,
  };

  return axios(config);
};

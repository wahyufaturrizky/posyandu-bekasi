/* eslint-disable */
import NetInfo from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import {Icon, Input} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import SweetAlert from 'react-native-sweet-alert';
import {iOSUIKit, materialColors} from 'react-native-typography';
import {AppBar} from '../../components/appBar/AppBar';
import {BasicLayoutNotCentered} from '../../components/basicLayout/BasicLayout';
import {ButtonPrimary, ButtonSecondary} from '../../components/button/Button';
import Loading from '../../components/loading';
import {
  updateDistrictAndSubdistrict,
  updateNamePosyandu,
  getSubDistrict,
  getDistrict,
} from '../../services/retrieveData';
import {ColorsPrimary} from '../../styles/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import SearchableDropdown from 'react-native-searchable-dropdown';

const Akun = (props) => {
  let titlePage = props.route.params.titlePage;
  const isFocused = useIsFocused();
  const {height} = Dimensions.get('window');
  const [isLoading, setIsLoading] = useState(false);
  const [isOffline, setOfflineStatus] = useState(false);
  const [isEditNote, setIsEditNote] = useState(false);
  const [dataListDistrict, setDataListDistrict] = useState([]);
  const [dataListSubDistrict, setDataListSubDistrict] = useState([]);
  const [selectDistrict, setSelectDistrict] = useState([]);
  const [selectSubDistrict, setSelectSubDistrict] = useState([]);
  const [dataNamePosyandu, setDataNamePosyandu] = useState({
    nama: 'Posyandu Melati 4',
    userid: '',
    rt: '1',
    rw: '',
  });
  const [dataDistrictAndSubdistrict, setdataDistrictAndSubdistrict] = useState({
    kode_kecamatan: '',
    kode_kelurahan: '',
    userid: '',
  });
  const [isStatusButtonUpdate, setIsStatusButtonUpdate] = useState('');

  const {nama, rt, rw} = dataNamePosyandu;

  useEffect(() => {
    if (isFocused || isStatusButtonUpdate === '') {
      checkConnectionIsOffline();
      fetchDataUserId();
      fetchDataDistrict();
      fetchDataSubDistrict();
    }
    if (isEditNote) {
      setIsStatusButtonUpdate('Save Update');
    }
    if (!isEditNote) {
      setIsStatusButtonUpdate('Update Note');
    }
  }, [isFocused, isEditNote, isStatusButtonUpdate]);

  const fetchDataUserId = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    setDataNamePosyandu({
      ...dataNamePosyandu,
      userid: data.userid,
      rw: data.rw,
    });
    setdataDistrictAndSubdistrict({
      ...dataDistrictAndSubdistrict,
      userid: data.userid,
    });
  };

  const fetchDataDistrict = async () => {
    setIsLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let dataHeaders = {
      tokenType: data.tokenType,
      accessToken: data.accessToken,
    };

    const response = await getDistrict(dataHeaders).catch((error) => {
      setIsLoading(false);
    });

    if (response) {
      const {status, data} = response;

      if (status === 200) {
        setIsLoading(false);
        setDataListDistrict(data.result);
      }
    }
  };

  const fetchDataSubDistrict = async () => {
    setIsLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let dataHeaders = {
      tokenType: data.tokenType,
      accessToken: data.accessToken,
    };

    const response = await getSubDistrict(dataHeaders).catch((error) => {
      setIsLoading(false);
    });

    if (response) {
      const {status, data} = response;

      if (status === 200) {
        setIsLoading(false);
        setDataListSubDistrict(data.result);
      }
    }
  };

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const handleChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const handleUpdateProfile = async () => {
    setIsLoading(true);

    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let dataHeaders = {
      tokenType: data.tokenType,
      accessToken: data.accessToken,
    };

    responseNamePosyandu = await updateNamePosyandu(dataHeaders).catch(
      (error) => {
        setIsLoading(false);
      },
    );

    responseNamePosyandu = await updateDistrictAndSubdistrict(
      dataHeaders,
    ).catch((error) => {
      setIsLoading(false);
    });

    if (
      responseNamePosyandu.status === 200 &&
      responseNamePosyandu.status === 200
    ) {
      setIsLoading(false);

      SweetAlert.showAlertWithOptions(
        {
          title: 'Berhasil update profil',
          subTitle: 'Yeay! kamu sukses update profil kamu',
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'success',
          cancellable: true,
        },
        (callback) =>
          props.navigation.navigate('HomePage', {
            titlePage: 'Beranda',
          }),
      );
    }
  };

  return (
    <>
      <AppBar onPress={() => props.navigation.goBack()} titlePage={titlePage} />
      <BasicLayoutNotCentered>
        <ScrollView>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: height * 0.04,
              flex: 1,
            }}>
            <View style={{position: 'relative'}}>
              <View
                style={{
                  backgroundColor: 'white',
                  borderRadius: 12,
                  paddingHorizontal: 14,
                  paddingVertical: 24,
                }}>
                <Text
                  style={[
                    iOSUIKit.subheadEmphasized,
                    {color: materialColors.blackPrimary},
                    {marginBottom: 14, marginLeft: isEditNote ? 0 : 14},
                  ]}>
                  {isEditNote ? 'Update Note' : 'Detail Akun'}
                </Text>
                {isEditNote ? (
                  <View>
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackSecondary},
                      ]}>
                      Nama Posyandu
                    </Text>
                    <Input
                      leftIcon={{
                        type: 'materialIcons',
                        name: 'subtitles',
                        color: ColorsPrimary.bluesoft,
                      }}
                      onChangeText={(val) => handleChange('nama', val)}
                      placeholder="please fill note title"
                      defaultValue={nama}
                    />
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackSecondary},
                      ]}>
                      RW
                    </Text>
                    <Input
                      leftIcon={{
                        type: 'materialIcons',
                        name: 'subtitles',
                        color: ColorsPrimary.bluesoft,
                      }}
                      onChangeText={(val) => handleChange('rw', val)}
                      placeholder="please fill note detail"
                      defaultValue={rw}
                    />
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackSecondary},
                      ]}>
                      RT
                    </Text>
                    <Input
                      leftIcon={{
                        type: 'materialIcons',
                        name: 'subtitles',
                        color: ColorsPrimary.bluesoft,
                      }}
                      onChangeText={(val) => handleChange('rt', val)}
                      placeholder="please fill note detail"
                      defaultValue={rt}
                    />
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackSecondary},
                      ]}>
                      Kecamatan
                    </Text>
                    <SearchableDropdown
                      onItemSelect={(item) => {
                        const items = selectDistrict;
                        items.push(item);
                        setSelectDistrict(items);
                        setState({...state, kode_kecamatan: item.value});
                      }}
                      containerStyle={{padding: 5}}
                      onRemoveItem={(item, index) => {
                        const items = selectDistrict.filter(
                          (sitem) => sitem.value !== item.value,
                        );
                        setSelectDistrict(items);
                      }}
                      itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#ddd',
                        borderColor: '#bbb',
                        borderWidth: 1,
                        borderRadius: 5,
                      }}
                      itemTextStyle={{color: '#222'}}
                      itemsContainerStyle={{maxHeight: 140}}
                      items={dataListDistrict}
                      defaultIndex={2}
                      resetValue={false}
                      textInputProps={{
                        placeholder: 'placeholder',
                        underlineColorAndroid: 'transparent',
                        style: {
                          padding: 12,
                          borderWidth: 1,
                          borderColor: '#ccc',
                          borderRadius: 5,
                        },
                        onTextChange: (text) => console.log(text),
                      }}
                      listProps={{
                        nestedScrollEnabled: true,
                      }}
                    />
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackSecondary, marginTop: 16},
                      ]}>
                      Kelurahan
                    </Text>
                    <SearchableDropdown
                      onItemSelect={(item) => {
                        const items = selectSubDistrict;
                        items.push(item);
                        setSelectSubDistrict(items);
                        setState({...state, kode_kelurahan: item.value});
                      }}
                      containerStyle={{padding: 5}}
                      onRemoveItem={(item, index) => {
                        const items = selectSubDistrict.filter(
                          (sitem) => sitem.value !== item.value,
                        );
                        setSelectSubDistrict(items);
                      }}
                      itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#ddd',
                        borderColor: '#bbb',
                        borderWidth: 1,
                        borderRadius: 5,
                      }}
                      itemTextStyle={{color: '#222'}}
                      itemsContainerStyle={{maxHeight: 140}}
                      items={dataListSubDistrict}
                      defaultIndex={2}
                      resetValue={false}
                      textInputProps={{
                        placeholder: 'placeholder',
                        underlineColorAndroid: 'transparent',
                        style: {
                          padding: 12,
                          borderWidth: 1,
                          marginBottom: 24,
                          borderColor: '#ccc',
                          borderRadius: 5,
                        },
                        onTextChange: (text) => console.log(text),
                      }}
                      listProps={{
                        nestedScrollEnabled: true,
                      }}
                    />
                  </View>
                ) : (
                  <>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderBottomColor: '#E5E5E5',
                        borderBottomWidth: 2,
                        paddingBottom: 8,
                        marginTop: 8,
                      }}>
                      <Icon
                        reverse
                        name="description"
                        type="materialIcons"
                        color={ColorsPrimary.bluesoft}
                      />
                      <View style={{marginLeft: 12}}>
                        <Text
                          textTransform="capitalize"
                          style={[
                            iOSUIKit.bodyEmphasized,
                            {color: materialColors.blackPrimary},
                          ]}>
                          Nama Posyandu
                        </Text>
                        <Text
                          textTransform="capitalize"
                          style={[
                            iOSUIKit.footnote,
                            {color: materialColors.blackTertiary},
                          ]}>
                          {nama || 'isian kosong'}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderBottomColor: '#E5E5E5',
                        borderBottomWidth: 2,
                        paddingBottom: 8,
                        marginTop: 8,
                      }}>
                      <Icon
                        reverse
                        name="description"
                        type="materialIcons"
                        color={ColorsPrimary.bluesoft}
                      />
                      <View style={{marginLeft: 12}}>
                        <Text
                          textTransform="capitalize"
                          style={[
                            iOSUIKit.bodyEmphasized,
                            {color: materialColors.blackPrimary},
                          ]}>
                          RW
                        </Text>
                        <Text
                          textTransform="capitalize"
                          style={[
                            iOSUIKit.footnote,
                            {color: materialColors.blackTertiary},
                          ]}>
                          {rw || 'isian kosong'}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderBottomColor: '#E5E5E5',
                        borderBottomWidth: 2,
                        paddingBottom: 8,
                        marginTop: 8,
                      }}>
                      <Icon
                        reverse
                        name="description"
                        type="materialIcons"
                        color={ColorsPrimary.bluesoft}
                      />
                      <View style={{marginLeft: 12}}>
                        <Text
                          textTransform="capitalize"
                          style={[
                            iOSUIKit.bodyEmphasized,
                            {color: materialColors.blackPrimary},
                          ]}>
                          RT
                        </Text>
                        <Text
                          textTransform="capitalize"
                          style={[
                            iOSUIKit.footnote,
                            {color: materialColors.blackTertiary},
                          ]}>
                          {rt || 'Isian kosong'}
                        </Text>
                      </View>
                    </View>
                  </>
                )}
              </View>
            </View>
          </View>
        </ScrollView>

        {isLoading && <Loading />}
      </BasicLayoutNotCentered>
      <ButtonSecondary
        customeStyle={{marginVertical: 16}}
        textButton={
          isStatusButtonUpdate === 'Save Update' ? 'Save Update' : 'Update Akun'
        }
        onPress={() => {
          setIsEditNote(!isEditNote);
          if (isStatusButtonUpdate === 'Save Update') {
            handleUpdateProfile();
          }
        }}
      />
      <ButtonPrimary
        customeStyle={{alignSelf: 'center', marginBottom: 16}}
        textButton={'Keluar'}
        onPress={() => {
          props.navigation.navigate('Login');
          AsyncStorage.removeItem('data');
          AsyncStorage.removeItem('password');
        }}
      />
    </>
  );
};

export default Akun;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});

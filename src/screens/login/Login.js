import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {Dimensions, View} from 'react-native';
import {TextInput} from 'react-native-paper';
import {BasicLayout} from '../../components/basicLayout/BasicLayout';
import {ButtonPrimary} from '../../components/button/Button';
import {ImageWarping} from '../../components/imageWarping/ImageWarping';
import Loading from '../../components/loading';
import {ColorsPrimary} from '../../styles/Colors';
import {postLogin} from '../../services/retrieveData';
import SweetAlert from 'react-native-sweet-alert';

const deviceHeight = Dimensions.get('screen').height;

const Login = (props) => {
  const [email, setEmail] = useState('posyandu-4@bekasikota.go.id');
  const [password, setPassword] = useState('1234567');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    checkLogin();
  }, []);

  const goToHome = () => {
    props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'HomePage', params: {titlePage: 'Beranda'}}],
      }),
    );
  };

  const checkLogin = async () => {
    let value = await AsyncStorage.getItem('data');
    if (value != null) {
      goToHome();
    }
  };

  const SignIn = async () => {
    if (email === '' || password === '') {
      SweetAlert.showAlertWithOptions(
        {
          title: 'Harap isi email dan password anda',
          subTitle:
            'Gagal masuk, karena email dan password belum di isi, harap coba kembali',
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          style: 'warning',
          cancellable: true,
        },
        (callback) => console.log('callback', callback),
      );
    } else {
      const data = {
        email: email,
        password: password,
      };
      setLoading(true);
      const response = await postLogin(data).catch((error) => {
        setLoading(false);

        const {data, status} = error.response;

        if (data) {
          if (data.msgCode === 'No such user found') {
            SweetAlert.showAlertWithOptions(
              {
                title: 'Gagal masuk',
                subTitle: `Email ${data.email}, tidak terdaftar`,
                confirmButtonTitle: 'OK',
                confirmButtonColor: '#000',
                style: 'error',
                cancellable: true,
              },
              (callback) => console.log('callback', callback),
            );
          }
          if (data.msgCode === 'Password is incorrect') {
            SweetAlert.showAlertWithOptions(
              {
                title: 'Gagal masuk',
                subTitle: 'Password yang Anda masukkan salah',
                confirmButtonTitle: 'OK',
                confirmButtonColor: '#000',
                style: 'error',
                cancellable: true,
              },
              (callback) => console.log('callback', callback),
            );
          }

          if (status === 502) {
            SweetAlert.showAlertWithOptions(
              {
                title: 'Gagal masuk',
                subTitle: `Error dengan status ${status}, segera hubungin admin di WA 62 822 7458 6011`,
                confirmButtonTitle: 'OK',
                confirmButtonColor: '#000',
                style: 'error',
                cancellable: true,
              },
              (callback) => console.log('callback', callback),
            );
          }
        }
      });

      if (response) {
        const {data, status} = response;

        if (status === 200) {
          setLoading(false);
          await AsyncStorage.setItem('data', JSON.stringify(data));
          await AsyncStorage.setItem('password', password);
          SweetAlert.showAlertWithOptions(
            {
              title: 'Berhasil masuk',
              subTitle: 'Email dan password anda sesuai',
              confirmButtonTitle: 'OK',
              confirmButtonColor: '#000',

              style: 'success',
              cancellable: true,
            },
            (callback) => goToHome(),
          );
        }
      }
    }
  };

  return (
    <BasicLayout isWithoutScroll>
      <View style={{marginBottom: 24}}>
        <ImageWarping
          resizeMode="contain"
          source={require('../../assets/images/logoPosyandu.png')}
        />
      </View>

      <TextInput
        placeholder="Email"
        value={email}
        mode="outlined"
        keyboardType="email-address"
        onChangeText={(val) => setEmail(val)}
        selectionColor={ColorsPrimary.blueDark}
        underlineColor={ColorsPrimary.blueDark}
        underlineColorAndroid={ColorsPrimary.blueDark}
        theme={{
          colors: {
            primary: ColorsPrimary.blueDark,
            underlineColor: 'transparent',
          },
          roundness: 32,
        }}
        style={{
          width: '90%',
          alignSelf: 'center',
          backgroundColor: 'white',
          marginBottom: deviceHeight * 0.02,
        }}
      />

      <TextInput
        placeholder="Password"
        value={password}
        mode="outlined"
        secureTextEntry={true}
        selectionColor={ColorsPrimary.blueDark}
        underlineColor={ColorsPrimary.blueDark}
        underlineColorAndroid={ColorsPrimary.blueDark}
        onChangeText={(val) => setPassword(val)}
        theme={{
          colors: {
            primary: ColorsPrimary.blueDark,
            underlineColor: 'transparent',
          },
          roundness: 32,
        }}
        style={{
          width: '90%',
          alignSelf: 'center',
          backgroundColor: 'white',
          marginBottom: deviceHeight * 0.02,
        }}
      />

      <ButtonPrimary textButton="Masuk" onPress={SignIn} />
      {loading && <Loading />}
    </BasicLayout>
  );
};

export default Login;

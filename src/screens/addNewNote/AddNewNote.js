/* eslint-disable */
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Input} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import SearchableDropdown from 'react-native-searchable-dropdown';
import SweetAlert from 'react-native-sweet-alert';
import {iOSUIKit, materialColors} from 'react-native-typography';
import {AppBar} from '../../components/appBar/AppBar';
import {BasicLayoutNotCentered} from '../../components/basicLayout/BasicLayout';
import {ButtonSecondary} from '../../components/button/Button';
import Loading from '../../components/loading';
import {
  getDistrict,
  getSubDistrict,
  postNewSummary,
} from '../../services/retrieveData';
import {ColorsPrimary} from '../../styles/Colors';

function AddNewNote(props) {
  let titlePage = props.route.params.titlePage;
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    rw: '',
    bulan: '',
    strata_posyandu: [],
    nama_posyandu: '',
    tahun: '',
    sasaran: '',
    kedatangan: '',
    ditimbang: '',
    naik: '',
    vitamin_a: '',
    bcg: '',
    dpt: '',
    polio: '',
    campak: '',
    hepatitis: '',
    diare: '',
    ibu_hamil: '',
    fe_tab: '',
    imunisasi_ibu_hamil: '',
    ibu_menyusui: '',
    kb: '',
    paud: '',
    bkb: '',
    bkr: '',
    bkl: '',
    up2k: '',
    dana_sehat: '',
    kader_pes_bpjs: '',
    bayi_stunting: '',
    kode_kecamatan: [],
    kode_kelurahan: [],
    kader_terlatih: '',
    kader_hadir: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [dataListDistrict, setDataListDistrict] = useState([]);
  const [dataListSubDistrict, setDataListSubDistrict] = useState([]);
  const [selectSubDistrict, setSelectSubDistrict] = useState([]);
  const [isOffline, setOfflineStatus] = useState(false);
  const [isSelectDistrict, setIsSelectDistrict] = useState(false);

  const dataStrataPosyandu = [
    {id: 1, name: 'pratama'},
    {id: 2, name: 'madya'},
    {id: 3, name: 'purnama'},
    {id: 4, name: 'mandiri'},
  ];

  const {
    rw,
    bulan,
    strata_posyandu,
    nama_posyandu,
    tahun,
    sasaran,
    kedatangan,
    ditimbang,
    naik,
    vitamin_a,
    bcg,
    dpt,
    polio,
    campak,
    hepatitis,
    diare,
    ibu_hamil,
    fe_tab,
    imunisasi_ibu_hamil,
    ibu_menyusui,
    kb,
    paud,
    bkb,
    bkr,
    bkl,
    up2k,
    dana_sehat,
    kader_pes_bpjs,
    bayi_stunting,
    kode_kecamatan,
    kode_kelurahan,
    kader_terlatih,
    kader_hadir,
  } = state;

  useEffect(() => {
    if (isFocused) {
      checkConnectionIsOffline();
      fetchDataDistrict();
      fetchDataSubDistrict();
    }
    if (isSelectDistrict) {
      setIsSelectDistrict(false);
      handleSelectDistrict();
    }
  }, [isFocused, isSelectDistrict]);

  const handleChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const fetchDataDistrict = async () => {
    setIsLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let dataHeaders = {
      tokenType: data.tokenType,
      accessToken: data.accessToken,
    };

    const response = await getDistrict(dataHeaders).catch((error) => {
      setIsLoading(false);
    });

    if (response) {
      const {status, data} = response;

      if (status === 200) {
        setIsLoading(false);
        const mapDistrict = data.result;
        let dataDistrict = mapDistrict.map((data, index) => ({
          id: data.value,
          name: data.label,
        }));
        setDataListDistrict(dataDistrict);
      }
    }
  };

  const fetchDataSubDistrict = async () => {
    setIsLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let dataHeaders = {
      tokenType: data.tokenType,
      accessToken: data.accessToken,
    };

    const response = await getSubDistrict(dataHeaders).catch((error) => {
      setIsLoading(false);
    });

    if (response) {
      const {status, data} = response;

      if (status === 200) {
        setIsLoading(false);
        const mapSubDistrict = data.result;
        let dataSubDistrict = mapSubDistrict.map((data, index) => ({
          id: data.value,
          name: data.label,
        }));
        setDataListSubDistrict(dataSubDistrict);
      }
    }
  };

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const handleAddNewSummary = async () => {
    const valueNote = [
      {
        name: 'RW',
        value: rw,
      },
      {
        name: 'Bulan',
        value: bulan,
      },
      {
        name: 'Strata Posyandu',
        value: strata_posyandu,
      },
      {
        name: 'Nama Posyandu',
        value: nama_posyandu,
      },
      {
        name: 'Tahun',
        value: tahun,
      },
      {
        name: 'Sasaran',
        value: sasaran,
      },
      {
        name: 'Kedatangan',
        value: kedatangan,
      },
      {
        name: 'Ditimbang',
        value: ditimbang,
      },
      {
        name: 'Naik',
        value: naik,
      },
      {
        name: 'Vitamin A',
        value: vitamin_a,
      },
      {
        name: 'Bcg',
        value: bcg,
      },
      {
        name: 'Dpt',
        value: dpt,
      },
      {
        name: 'Polio',
        value: polio,
      },
      {
        name: 'Campak',
        value: campak,
      },
      {
        name: 'Hepatitis',
        value: hepatitis,
      },
      {
        name: 'Diare',
        value: diare,
      },
      {
        name: 'Ibu Hamil',
        value: ibu_hamil,
      },
      {
        name: 'Fe Tab',
        value: fe_tab,
      },
      {
        name: 'Imunisasi Ibu Hamil',
        value: imunisasi_ibu_hamil,
      },
      {
        name: 'Ibu Menyusui',
        value: ibu_menyusui,
      },
      {
        name: 'KB',
        value: kb,
      },
      {
        name: 'Paud',
        value: paud,
      },
      {
        name: 'Bkb',
        value: bkb,
      },
      {
        name: 'Bkr',
        value: bkr,
      },
      {
        name: 'Bkl',
        value: bkl,
      },
      {
        name: 'Up2k',
        value: up2k,
      },
      {
        name: 'Dana Sehat',
        value: dana_sehat,
      },
      {
        name: 'Kader Pes Bpjs Sehat',
        value: kader_pes_bpjs,
      },
      {
        name: 'Bayi Stunting',
        value: bayi_stunting,
      },
      {
        name: 'Kader Terlatih',
        value: kader_terlatih,
      },
      {
        name: 'Kader Hadir',
        value: kader_hadir,
      },
      {
        name: 'Kecamatan',
        value: kode_kecamatan,
      },
      {
        name: 'Kelurahan',
        value: kode_kelurahan,
      },
    ];

    let emptyField = valueNote.find((value) => value.value === '');

    if (!emptyField) {
      setIsLoading(true);
      const value = await AsyncStorage.getItem('data');
      const data = JSON.parse(value);

      let dataHeaders = {
        tokenType: data.tokenType,
        accessToken: data.accessToken,
      };

      const response = await postNewSummary(state, dataHeaders).catch(
        (error) => {
          setIsLoading(false);
          SweetAlert.showAlertWithOptions(
            {
              title: 'Gagal menambahkan laporan',
              subTitle: `kesalahan jaringan internet`,
              confirmButtonTitle: 'OK',
              confirmButtonColor: '#000',
              otherButtonTitle: 'Cancel',
              otherButtonColor: '#dedede',
              style: 'success',
              cancellable: true,
            },
            (callback) => console.log('callback', callback),
          );
        },
      );

      const {status} = response;

      if (status === 201) {
        setIsLoading(false);

        SweetAlert.showAlertWithOptions(
          {
            title: 'Berhasil menambahkan laporan',
            subTitle: 'Yeay! kamu sukses menambah data laporan',
            confirmButtonTitle: 'OK',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'success',
            cancellable: true,
          },
          (callback) =>
            props.navigation.navigate('HomePage', {
              titlePage: 'Beranda',
            }),
        );
      }
    } else {
      SweetAlert.showAlertWithOptions(
        {
          title: 'Perhatian!',
          subTitle: `isian ini ${emptyField.name} tidak boleh kosong`,
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'warning',
          cancellable: true,
        },
        (callback) => console.log('callback', callback),
      );
    }
  };

  const handleSelectDistrict = (data) => {};

  return (
    <>
      <AppBar titlePage={titlePage} onPress={() => props.navigation.goBack()} />
      <BasicLayoutNotCentered>
        <View style={styles.warpingCentered}>
          <View style={styles.cardWarping}>
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary},
              ]}>
              Kecamatan
            </Text>
            <SearchableDropdown
              onTextChange={(text) => console.log('asdasd', text)}
              onItemSelect={(item) => {
                setIsSelectDistrict(true);
                handleSelectDistrict(item);
              }}
              containerStyle={{padding: 5}}
              itemStyle={{
                padding: 10,
                marginTop: 2,
                backgroundColor: '#ddd',
                borderColor: '#bbb',
                borderWidth: 1,
                borderRadius: 5,
              }}
              itemTextStyle={{color: '#222'}}
              // itemsContainerStyle={{maxHeight: 140}}
              items={dataListDistrict}
              defaultIndex={2}
              resetValue={false}
              textInputProps={{
                placeholder: 'Pilih kecamatan',
                underlineColorAndroid: 'transparent',
                style: {
                  padding: 12,
                  borderWidth: 1,
                  borderColor: '#ccc',
                  borderRadius: 5,
                },
              }}
              listProps={{
                nestedScrollEnabled: true,
              }}
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary, marginTop: 16},
              ]}>
              Kelurahan
            </Text>
            <SearchableDropdown
              selectedItems={kode_kelurahan}
              onItemSelect={(item) => {
                console.log('asdasdas', item);
                const items = kode_kelurahan;
                items.push(item);
                setState({...state, kode_kelurahan: items});
              }}
              containerStyle={{padding: 5}}
              onRemoveItem={(item, index) => {
                const items = selectSubDistrict.filter(
                  (sitem) => sitem.code !== item.code,
                );
                setState({...state, kode_kelurahan: items});
              }}
              itemStyle={{
                padding: 10,
                marginTop: 2,
                backgroundColor: '#ddd',
                borderColor: '#bbb',
                borderWidth: 1,
                borderRadius: 5,
              }}
              itemTextStyle={{color: '#222'}}
              // itemsContainerStyle={{maxHeight: 140}}
              items={dataListSubDistrict}
              defaultIndex={2}
              resetValue={false}
              textInputProps={{
                placeholder: 'Pilih kelurahan',
                underlineColorAndroid: 'transparent',
                style: {
                  padding: 12,
                  borderWidth: 1,

                  borderColor: '#ccc',
                  borderRadius: 5,
                },
                onTextChange: (text) => console.log(text),
              }}
              listProps={{
                nestedScrollEnabled: true,
              }}
            />
            <Text
              style={[
                iOSUIKit.footnote,
                {color: materialColors.blackSecondary, marginTop: 16},
              ]}>
              Strata Posyandu
            </Text>
            <SearchableDropdown
              selectedItems={strata_posyandu}
              onItemSelect={(item) => {
                const items = strata_posyandu;
                items.push(item);
                setState({...state, strata_posyandu: items});
              }}
              containerStyle={{padding: 5}}
              onRemoveItem={(item, index) => {
                const items = selectSubDistrict.filter(
                  (sitem) => sitem.id !== item.id,
                );
                setState({...state, strata_posyandu: items});
              }}
              itemStyle={{
                padding: 10,
                marginTop: 2,
                backgroundColor: '#ddd',
                borderColor: '#bbb',
                borderWidth: 1,
                borderRadius: 5,
              }}
              itemTextStyle={{color: '#222'}}
              // itemsContainerStyle={{maxHeight: 140}}
              items={dataStrataPosyandu}
              defaultIndex={2}
              resetValue={false}
              textInputProps={{
                placeholder: 'Pilih kelurahan',
                underlineColorAndroid: 'transparent',
                style: {
                  padding: 12,
                  borderWidth: 1,
                  marginBottom: 24,
                  borderColor: '#ccc',
                  borderRadius: 5,
                },
                onTextChange: (text) => console.log(text),
              }}
              listProps={{
                nestedScrollEnabled: true,
              }}
            />
          </View>
        </View>
        <ScrollView keyboardShouldPersistTaps="always">
          <View style={styles.warpingCentered}>
            <View style={styles.cardWarping}>
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                RW
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('rw', val)}
                placeholder="Masukkan data RW"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Bulan
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('bulan', val)}
                placeholder="Masukkan data bulan"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Nama Posyandu
              </Text>
              <Input
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('nama_posyandu', val)}
                placeholder="Masukkan data Nama Posyandu"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Tahun
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('tahun', val)}
                placeholder="Masukkan Tahun"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Sasaran
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('sasaran', val)}
                placeholder="Masukkan data sasaran"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Kedatangan
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('kedatangan', val)}
                placeholder="Masukkan data Kedatangan"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Ditimbang
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('ditimbang', val)}
                placeholder="Masukkan data timbangan"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Naik
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('naik', val)}
                placeholder="Masukkan data kenaikan"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Vitamin A
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('vitamin_a', val)}
                placeholder="Masukkan data Vitamin A"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Bcg
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('bcg', val)}
                placeholder="Masukkan data Bcg"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Dpt
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('dpt', val)}
                placeholder="Masukkan data Dpt"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Polio
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('polio', val)}
                placeholder="Masukkan data Polio"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Campak
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('campak', val)}
                placeholder="Masukkan data Campak"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Hepatitis
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('hepatitis', val)}
                placeholder="Masukkan data Hepatitis"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Diare
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('diare', val)}
                placeholder="Masukkan data Diare"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Ibu Hamil
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('ibu_hamil', val)}
                placeholder="Masukkan data Ibu Hamil"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Fe Tab
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('fe_tab', val)}
                placeholder="Masukkan data Fe Tab"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Imunisasi Ibu Hamil
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('imunisasi_ibu_hamil', val)}
                placeholder="Masukkan data Imunisasi Ibu Hamil"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Ibu Menyusui
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('ibu_menyusui', val)}
                placeholder="Masukkan data Ibu Menyusui"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                KB
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('kb', val)}
                placeholder="Masukkan data KB"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Paud
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('paud', val)}
                placeholder="Masukkan data Paud"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Bkb
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('bkb', val)}
                placeholder="Masukkan data Bkb"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Bkr
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('bkr', val)}
                placeholder="Masukkan data Bkr"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Bkl
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('bkl', val)}
                placeholder="Masukkan data Bkl"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Up2k
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('up2k', val)}
                placeholder="Masukkan data Up2k"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Dana Sehat
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('dana_sehat', val)}
                placeholder="Masukkan data  Dana Sehat"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Kader Pes Bpjs
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('kader_pes_bpjs', val)}
                placeholder="Masukkan data Kader Pes Bpjs"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Bayi Stunting
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('bayi_stunting', val)}
                placeholder="Masukkan data Bayi Stunting"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Kader Terlatih
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('kader_terlatih', val)}
                placeholder="Masukkan data Kader Terlatih"
              />
              <Text
                style={[
                  iOSUIKit.footnote,
                  {color: materialColors.blackSecondary},
                ]}>
                Kader Hadir
              </Text>
              <Input
                keyboardType="number-pad"
                leftIcon={{
                  type: 'materialIcons',
                  name: 'subtitles',
                  color: ColorsPrimary.bluesoft,
                }}
                onChangeText={(val) => handleChange('kader_hadir', val)}
                placeholder="Masukkan data Kader Hadir"
              />
            </View>
          </View>
        </ScrollView>

        {isLoading && <Loading />}
      </BasicLayoutNotCentered>
      <ButtonSecondary textButton="Simpan" onPress={handleAddNewSummary} />
    </>
  );
}

export default AddNewNote;

const styles = StyleSheet.create({
  warpingCentered: {
    width: '90%',
    alignSelf: 'center',
    flex: 1,
    paddingTop: 32,
  },
  cardWarping: {
    backgroundColor: ColorsPrimary.white,
    borderRadius: 12,
    paddingHorizontal: 14,
    paddingVertical: 24,
  },
});

import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  RefreshControl,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import SweetAlert from 'react-native-sweet-alert';
import Toast from 'react-native-toast-message';
import {iOSUIKit, materialColors} from 'react-native-typography';
import {BasicLayoutNotCentered} from '../../components/basicLayout/BasicLayout';
import BottomTab from '../../components/bottomTab/BottomTab';
import Loading from '../../components/loading';
import {getSummary} from '../../services/retrieveData';
import {ColorsPrimary} from '../../styles/Colors';
import {AppBar} from '../../components/appBar/AppBar';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

const Summary = (props) => {
  let titlePage = props.route.params.titlePage;
  const isFocused = useIsFocused();
  const {height} = Dimensions.get('window');
  const [dataListSummary, setDataListSummary] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    if (isFocused) {
      fetchDataListSummary();
      checkConnectionIsOffline();
    }
  }, [isFocused]);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    fetchDataListSummary();

    wait(2000).then(() => setRefreshing(false));
  }, []);

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const fetchDataListSummary = async () => {
    const value = await AsyncStorage.getItem('data');
    const password = await AsyncStorage.getItem('password');
    const data = JSON.parse(value);

    let dataHeaders = {
      kode_kelurahan: data.kode_kelurahan,
      tokenType: data.tokenType,
      accessToken: data.accessToken,
      rw: data.rw,
    };

    let dataForm = {
      email: data.email,
      password: password,
      firstName: 'User',
      lastName: 'Posyandu',
    };

    setIsLoading(true);
    response = await getSummary(dataForm, dataHeaders).catch((error) => {
      const {data} = error.response;
      setIsLoading(false);
      Toast.show({
        text1: 'Gagal menarik data laporan',
        text2: 'harap coba refresh kembali 👋',
        type: 'error',
      });
    });

    if (response) {
      const {status, data} = response;

      if (status === 201) {
        Toast.show({
          text1: 'Berhasil menarik data laporan',
          text2: 'Daftar laporan Anda 👋',
        });
        setDataListSummary(data.result);
        setIsLoading(false);
      }
    }
  };

  return (
    <>
      <AppBar titlePage={titlePage} onPress={() => props.navigation.goBack()} />
      <BasicLayoutNotCentered isWithoutScroll>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: height * -0.02,
              flex: 1,
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 30,
                marginBottom: 16,
              }}>
              <Text
                style={[
                  iOSUIKit.subheadEmphasized,
                  {color: materialColors.blackSecondary},
                ]}>
                Daftar laporan anda
              </Text>
            </View>

            <View style={{position: 'relative', marginBottom: 80}}>
              <View
                style={{
                  backgroundColor: 'white',
                  borderRadius: 12,
                  paddingHorizontal: 14,
                  paddingVertical: 24,
                }}>
                <Text
                  style={[
                    iOSUIKit.subheadEmphasized,
                    {color: materialColors.blackPrimary},
                    {marginBottom: 14, marginLeft: 14},
                  ]}>
                  Total laporan berjumlah: {dataListSummary.length}
                </Text>
                {dataListSummary.length !== 0 ? (
                  dataListSummary.map((value, key) => {
                    const {nama_posyandu, strata_posyandu} = value;
                    return (
                      <TouchableOpacity
                        key={key}
                        onPress={() =>
                          props.navigation.navigate('DetailNote', {
                            value,
                            titlePage: 'Detail Laporan',
                          })
                        }>
                        <View>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                              borderBottomColor: '#E5E5E5',
                              borderBottomWidth: 2,
                              paddingBottom: 8,
                              marginTop: 8,
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}>
                              <Icon
                                reverse
                                name="description"
                                type="materialIcons"
                                color={ColorsPrimary.bluesoft}
                              />
                              <View style={{marginLeft: 12}}>
                                <Text
                                  textTransform="capitalize"
                                  style={[
                                    iOSUIKit.bodyEmphasized,
                                    {color: materialColors.blackPrimary},
                                  ]}>
                                  {nama_posyandu || 'empty title'}
                                </Text>
                                <Text
                                  textTransform="capitalize"
                                  style={[
                                    iOSUIKit.footnote,
                                    {color: materialColors.blackTertiary},
                                  ]}>
                                  {strata_posyandu || 'empty detail'}
                                </Text>
                              </View>
                            </View>
                            <View>
                              <Text
                                textTransform="capitalize"
                                style={[
                                  iOSUIKit.footnote,
                                  {color: ColorsPrimary.bluesoft},
                                ]}>
                                See detail
                              </Text>
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    );
                  })
                ) : (
                  <View>
                    <Text
                      style={[
                        iOSUIKit.footnote,
                        {color: materialColors.blackTertiary},
                        {
                          justifyContent: 'center',
                          textAlign: 'center',
                        },
                      ]}>
                      {isOffline
                        ? 'Ooppss your offline... '
                        : 'Please wait get all data notes ...'}
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={{position: 'absolute', bottom: 64, right: 16}}>
          <Icon
            reverse
            name="plus"
            type="font-awesome"
            color="#F54291"
            onPress={() =>
              props.navigation.navigate('AddNewNote', {
                titlePage: 'Tambah Laporan Baru',
              })
            }
          />
        </View>

        <BottomTab {...props} />
      </BasicLayoutNotCentered>
      <Toast ref={(ref) => Toast.setRef(ref)} />
      {isLoading && <Loading />}
    </>
  );
};

export default Summary;

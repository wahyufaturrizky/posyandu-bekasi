import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Text, View} from 'react-native';
import {material} from 'react-native-typography';
import {BasicLayout} from '../../components/basicLayout/BasicLayout';
import {ButtonPrimary} from '../../components/button/Button';
import {ImageWarping} from '../../components/imageWarping/ImageWarping';

const Onboarding = (props) => {
  useEffect(() => {
    checkLogin();
  }, []);

  const goToHome = () => {
    props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'HomePage'}],
      }),
    );
  };

  const checkLogin = async () => {
    let value = await AsyncStorage.getItem('data');
    if (value != null) {
      goToHome();
    }
  };

  return (
    <BasicLayout isWithoutScroll>
      <ImageWarping
        resizeMode="contain"
        source={require('../../assets/images/logoPosyandu.png')}
      />
      <View style={{marginVertical: 24}}>
        <Text style={material.headlineWhite}>Posyandu Bekasi</Text>
      </View>
      <ButtonPrimary
        textButton="LANJUTKAN"
        onPress={() => props.navigation.navigate('Login')}
      />
    </BasicLayout>
  );
};

export default Onboarding;

/* eslint-disable */
import NetInfo from '@react-native-community/netinfo';
import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon, Input} from 'react-native-elements';
import SweetAlert from 'react-native-sweet-alert';
import {iOSUIKit, materialColors} from 'react-native-typography';
import {AppBar} from '../../components/appBar/AppBar';
import {BasicLayoutNotCentered} from '../../components/basicLayout/BasicLayout';
import Loading from '../../components/loading';
import {addNewNote, deleteSummaryById} from '../../services/retrieveData';
import {ColorsPrimary} from '../../styles/Colors';
import AsyncStorage from '@react-native-community/async-storage';

function DetailNote(props) {
  let dataById = props.route.params.value;
  let titlePage = props.route.params.titlePage;
  const isFocused = useIsFocused();
  const {height} = Dimensions.get('window');
  const [isLoading, setIsLoading] = useState(false);
  const [isOffline, setOfflineStatus] = useState(false);
  const [isEditNote, setIsEditNote] = useState(false);
  const [isStatusButtonUpdate, setIsStatusButtonUpdate] = useState('');

  useEffect(() => {
    if (isFocused || isStatusButtonUpdate === '') {
      checkConnectionIsOffline();
    }
    if (isEditNote) {
      setIsStatusButtonUpdate('Save Update');
    }
    if (!isEditNote) {
      setIsStatusButtonUpdate('Update Note');
    }
  }, [isFocused, isEditNote, isStatusButtonUpdate]);

  const checkConnectionIsOffline = () => {
    NetInfo.addEventListener((networkState) => {
      const offline =
        !networkState.isConnected && !networkState.isInternetReachable;
      if (offline) {
        setOfflineStatus(offline);
        SweetAlert.showAlertWithOptions(
          {
            title: 'Connection Error',
            subTitle:
              'Oops! Looks like your device is not connected to the internet',
            confirmButtonTitle: 'Retry to Connect',
            confirmButtonColor: '#000',
            otherButtonTitle: 'Cancel',
            otherButtonColor: '#dedede',
            style: 'warning',
            cancellable: true,
          },
          (callback) => console.log('callback', callback),
        );
      }
    });
  };

  const handleChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const handleDeleteSummaryById = async (dataSummary) => {
    setIsLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let dataHeaders = {
      tokenType: data.tokenType,
      accessToken: data.accessToken,
    };

    let dataForm = {
      lastName: dataSummary.nama_posyandu,
    };

    response = await deleteSummaryById(
      dataHeaders,
      dataForm,
      dataSummary.id,
    ).catch((error) => {
      setIsLoading(false);
      SweetAlert.showAlertWithOptions(
        {
          title: 'Gagal menghapus data',
          subTitle: `kesalahan jaringan internet`,
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'error',
          cancellable: true,
        },
        (callback) => console.log('callback', callback),
      );
    });

    const {status} = response;

    if (status === 201) {
      setIsLoading(false);

      SweetAlert.showAlertWithOptions(
        {
          title: 'Berhasil delete data laporan',
          subTitle: 'Yeay! kamu berhasil menghapus data laporan',
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'success',
          cancellable: true,
        },
        (callback) => props.navigation.navigate('HomePage'),
      );
    }
  };

  const handleUpdateNote = async () => {
    setIsLoading(true);

    const data = {
      title: title,
      detail: detail,
      noteId: _id,
    };

    response = await addNewNote(data).catch((error) => {
      setIsLoading(false);
    });

    const {status} = response;

    if (status === 200) {
      setIsLoading(false);

      SweetAlert.showAlertWithOptions(
        {
          title: 'Success Update',
          subTitle: 'Yeay! Your success update note',
          confirmButtonTitle: 'OK',
          confirmButtonColor: '#000',
          otherButtonTitle: 'Cancel',
          otherButtonColor: '#dedede',
          style: 'success',
          cancellable: true,
        },
        (callback) => props.navigation.navigate('HomePage'),
      );
    }
  };

  return (
    <>
      <BasicLayoutNotCentered>
        <AppBar
          titlePage={titlePage}
          onPress={() => props.navigation.goBack()}
        />

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * 0.04,
            flex: 1,
          }}>
          <View style={{position: 'relative'}}>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 12,
                paddingHorizontal: 14,
                paddingVertical: 24,
              }}>
              <Text
                style={[
                  iOSUIKit.subheadEmphasized,
                  {color: materialColors.blackPrimary},
                  {marginBottom: 14, marginLeft: 14},
                ]}>
                {isEditNote ? 'Update Note' : 'Detail Laporan'}
              </Text>
              {isEditNote ? (
                <View>
                  <Text
                    style={[
                      iOSUIKit.footnote,
                      {color: materialColors.blackSecondary},
                    ]}>
                    Title
                  </Text>
                  <Input
                    leftIcon={{
                      type: 'materialIcons',
                      name: 'book',
                      color: '#46B5A7',
                    }}
                    onChangeText={(val) => handleChange('title', val)}
                    placeholder="please fill note title"
                  />
                  <Text
                    style={[
                      iOSUIKit.footnote,
                      {color: materialColors.blackSecondary},
                    ]}>
                    Detail
                  </Text>
                  <Input
                    leftIcon={{
                      type: 'materialIcons',
                      name: 'subtitles',
                      color: '#46B5A7',
                    }}
                    onChangeText={(val) => handleChange('detail', val)}
                    placeholder="please fill note detail"
                  />
                </View>
              ) : (
                <>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Nama Posyandu
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.nama_posyandu}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Bayi Sunting
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.bayi_stunting}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        BCG
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.bcg}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        BKB
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.bkb}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        BKL
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.bkl}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        BKR
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.bkr}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Bulan
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.bulan}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Campak
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.campak}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Dana Sehat
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.dana_sehat}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Diare
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.diare}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Ditimbang
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.ditimbang}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        DPT
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.dpt}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        FE TAB
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.fe_tab}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Hepatitis
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.hepatitis}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Ibu Hamil
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.ibu_hamil}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Ibu Menyusui
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.ibu_menyusui}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Imunisasi Ibu Hamil
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.imunisasi_ibu_hamil}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Kader Hadir
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.kader_hadir}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Kader Pes BPJS
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.kader_pes_bpjs}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Kader Terlatih
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.kader_terlatih}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        KB
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.kb}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Kedatangan
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.kedatangan}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Naik
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.naik}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Paud
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.paud}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Polio
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.polio}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        RW
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.rw}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Sasaran
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.sasaran}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Strata Posyandu
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.strata_posyandu}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Tahun
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.tahun}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Up2k
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.up2k}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor: '#E5E5E5',
                      borderBottomWidth: 2,
                      paddingBottom: 8,
                      marginTop: 8,
                    }}>
                    <Icon
                      reverse
                      name="description"
                      type="materialIcons"
                      color={ColorsPrimary.bluesoft}
                    />
                    <View style={{marginLeft: 12}}>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.bodyEmphasized,
                          {color: materialColors.blackPrimary},
                        ]}>
                        Vitamin A
                      </Text>
                      <Text
                        textTransform="capitalize"
                        style={[
                          iOSUIKit.footnote,
                          {color: materialColors.blackTertiary},
                        ]}>
                        {dataById?.vitamin_a}
                      </Text>
                    </View>
                  </View>
                </>
              )}
            </View>
          </View>
        </View>

        {/* <TouchableOpacity
            onPress={() => {
              setIsEditNote(!isEditNote);
              if (isStatusButtonUpdate === 'Save Update') {
                handleUpdateNote();
              }
            }}
            style={{
              width: '90%',
              padding: 15,
              borderRadius: 12,
              alignSelf: 'center',
              backgroundColor: '#46B5A7',
              marginBottom: 14,
            }}>
            <Text
              style={[
                {
                  textAlign: 'center',
                },
                iOSUIKit.title3White,
              ]}>
              {isStatusButtonUpdate === 'Save Update'
                ? 'Save Update'
                : 'Update note'}
            </Text>
          </TouchableOpacity> */}
      </BasicLayoutNotCentered>
      <View style={{backgroundColor: 'white'}}>
        <TouchableOpacity
          onPress={() => handleDeleteSummaryById(dataById)}
          style={{
            width: '90%',
            padding: 15,
            borderRadius: 12,
            alignSelf: 'center',
            backgroundColor: ColorsPrimary.redSoft,
            marginVertical: 14,
          }}>
          <Text
            style={[
              {
                textAlign: 'center',
              },
              iOSUIKit.title3White,
            ]}>
            Hapus Laporan
          </Text>
        </TouchableOpacity>
      </View>
      {isLoading && <Loading />}
    </>
  );
}

export default DetailNote;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
